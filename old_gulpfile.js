/**
 * Project config
 */
const projectName = 'Banana Republic Frontend';
const projectProxyUrl = 'banana-republic.local';

/**
 * Styles config
 */
const styleSRC = './src/scss/style.scss';
const styleDestination = './dist/css/';

/**
 * Custom JS config
 */
const jsCustomSRC = './src/js/custom/*.js';
const jsCustomDestination = './dist/js/';
const jsCustomFilename = 'custom';

/**
 * Watch file(s) config
 */
const jsCustomWatchFiles = jsCustomSRC;
const styleWatchFiles = styleSRC;

/**
 * A list of browsers that we would like to include vendor prefixes for.
 * @see https://github.com/browserslist/browserslist
 */
const AUTOPREFIX_BROWSER_LIST = [
  'last 2 version',
  'bb >= 10',
  '> 2%',
  'android >= 4',
  'ios >= 7',
  'opera >= 23',
  'safari >= 7',
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
];

/**
 * Plugins
 */
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const mmq = require('gulp-merge-media-queries');
const uglifycss = require('gulp-uglifycss');
const rename = require('gulp-rename');
const lineec = require('gulp-line-ending-corrector');
const sourcemaps = require('gulp-sourcemaps');
const notify = require('gulp-notify');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload; // For manual browser reload.
const filter = require('gulp-filter');

/**
 * Task: Watch
 * 
 * Watches for file changes
 */
gulp.task('default', ['customJS', 'browser-sync', 'styles'], () => {
  gulp.watch(jsCustomWatchFiles, ['customJS', reload]);
  gulp.watch(styleWatchFiles, ['styles']);
});

gulp.task('browser-sync', () => {
  browserSync.init({
    // proxy: projectProxyUrl,
    open: true,
    injectChanges: true
  })
});
/**
 * Task: CustomJs
 * 
 * Concatenates and uglifies js scripts
 */
gulp.task('customJS', () => {
  gulp.src(jsCustomSRC)
    .pipe(concat(jsCustomFilename + '.js'))
    .pipe(lineec())
    .pipe(gulp.dest(jsCustomDestination))
    .pipe(rename({
      basename: jsCustomFilename,
      suffix: '.min'
    }))
    .pipe(uglify())
    .pipe(lineec())
    .pipe(gulp.dest(jsCustomDestination))
    .pipe(notify({
      message: 'TASK: "customJs" Completed!',
      onLast: true
    }));
});

gulp.task('styles', () => {
  gulp.src(styleSRC)
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'compact',
    errLogToConsole: true,
    precision: 10
  }))
  .on('error', console.error.bind(console))
  .pipe(sourcemaps.write({
    includeContent: false
  }))
  .pipe(
    sourcemaps.init({
      loadMaps: true
    })
  )
  .pipe(autoprefixer(AUTOPREFIX_BROWSER_LIST))
  .pipe(sourcemaps.write(styleDestination))
  .pipe(filter('**/*.css'))
  .pipe(mmq({
    log: true
  }))
  .pipe(browserSync.stream())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(uglifycss({
    maxLineLen: 10
  }))
  .pipe(lineec())
  .pipe(gulp.dest(styleDestination))
  .pipe(filter('**/*.css'))
  .pipe(browserSync.stream());
});